import React from 'react';
import { ContextIndex } from './components/Context/ContextIndex';
// import { FVIndex } from './components/FormValidation/FVIndex';
// import { LoginIndex } from './components/LoginForm/LoginIndex';
// import { RouterIndex } from './components/ReactRouter/RouterIndex';
// import { TAIndex } from './components/TableAxios/TAIndex';
// import { TMIndex } from './components/TableMock/TMIndex';
// import { CPFIndex } from './components/ColorPickerRefactor/CPFIndex';
// import { CPIndex } from './components/ColorPicker/CPIndex';
// import ENIndex from './components/Enable/ENIndex';
// import REIndex from './components/Refactor/REIndex';
// import HelloReact from './components/HelloReact/Hello'
// import StateIndex from './components/State/StateIndex';
// import CBIndex from './components/CallBack/CBIndex';
// import { SBIndex } from './components/Sidebar/SBIndex';

class App extends React.Component {
  public render() {
    return <div>
      <ContextIndex />
      {/* <FVIndex /> */}
      {/* <LoginIndex /> */}
      {/* <RouterIndex /> */}
      {/* <TAIndex /> */}
      {/* <TMIndex /> */}
      {/* <SBIndex /> */}
      {/* <CPFIndex /> */}
      {/* <CPIndex /> */}
      {/* <ENIndex /> */}
      {/* <REIndex /> */}
      {/* <CBIndex /> */}
      {/* <HelloReact userName="User1" />
      <StateIndex /> */}
    </div>
  }
}
// function App() {
//   return (
//     <div className="App">
//       <Demo name="4hhg" />
//     </div>
//   );
// }

export default App;
