import * as React from "react";
import { CPHello } from "./CPHello";
import { CPNameEdit } from "./CPNameEdit";
import { CPBrowser } from "./CPBrowser";
import { CP } from "./CP";
import { Color } from "./Model/color";

export const CPIndex = () => {
  const [name, setName] = React.useState("defaultUserName");
  const [editingName, setEditingName] = React.useState("defaultUserName");
  const [color, setColor] = React.useState<Color>({
    red: 20,
    green: 40,
    blue: 180
  });

  const loadUsername = () => {
    setTimeout(() => {
      setName("name from async call");
      setEditingName("name from async call");
    }, 500);
  };

  React.useEffect(() => {
    loadUsername();
  }, []);

  const setUsernameState = () => {
    setName(editingName);
  };

  return (
    <>
      <CPBrowser color={color} />
      <CP color={color} onColorUpdated={setColor}/>
      <CPHello userName={name} />
      <CPNameEdit
        initialUserName={name}
        editingName={editingName}
        onNameUpdated={setUsernameState}
        onEditingNameUpdated={setEditingName}
        disabled={editingName === "" || editingName === name}
      />
    </>
  );
};