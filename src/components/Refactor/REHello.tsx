import * as React from "react";

interface Props {
  userName: string;
}

const REHello: React.FC<Props> = (props) => {
  return <h2>Hello user: {props.userName} !</h2>;
};

export default REHello;