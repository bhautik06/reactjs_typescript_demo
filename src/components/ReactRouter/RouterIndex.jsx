import * as React from "react";
import { HashRouter, Switch, Route } from "react-router-dom";
import { PageA } from "./Pages/PageA";
import { PageB } from "./Pages/PageB";

export const RouterIndex = () => {
  return (
    <>
      <HashRouter>
        <Switch>
          <Route exact={true} path="/" component={PageA} />
          <Route path="/pageB" component={PageB} />
        </Switch>
      </HashRouter>
    </>
  );
};
