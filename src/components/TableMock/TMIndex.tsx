import * as React from "react";
import { TMHello } from "./TMHello";
import { TMNameEdit } from "./TMNameEdit";
import { TMBrowser } from "./TMBrowser";
import { TMColorPicker } from "./TMColorPicker";
import { TMSidebar } from "./TMSidebar";
import { TMMemberTable } from "./TMMemberTable"
import { Color } from "./Model/color";

export const TMIndex = () => {
  const [name, setName] = React.useState("defaultUserName");
  const [editingName, setEditingName] = React.useState("defaultUserName");
  const [color, setColor] = React.useState<Color>({
    red: 20,
    green: 40,
    blue: 180
  });
  const [isVisible, setVisible] = React.useState(false);

  const loadUsername = () => {
    setTimeout(() => {
      setName("name from async call");
      setEditingName("name from async call");
    }, 500);
  };

  React.useEffect(() => {
    loadUsername();
  }, []);

  const setUsernameState = () => {
    setName(editingName);
  };

  return (
    <>
      <TMSidebar isVisible={isVisible}>
        <h1>Cool Scfi movies</h1>
        <ul>
          <li>
            <a href="https://www.imdb.com/title/tt0816692/">Interstellar</a>
          </li>
          <li>
            <a href="https://www.imdb.com/title/tt0083658/">Blade Runner</a>
          </li>
          <li>
            <a href="https://www.imdb.com/title/tt0062622/">
              2001: a space odyssey
            </a>
          </li>
        </ul>
      </TMSidebar>
      <TMMemberTable />
      <TMBrowser color={color} />
      <TMColorPicker color={color} onColorUpdated={setColor} />
      <TMHello userName={name} />
      <TMNameEdit
        initialUserName={name}
        editingName={editingName}
        onNameUpdated={setUsernameState}
        onEditingNameUpdated={setEditingName}
        disabled={editingName === "" || editingName === name}
      />
      <div style={{ float: "right" }}>
        <button onClick={() => setVisible(!isVisible)}>Toggle Sidebar</button>
      </div>
    </>
  );
};