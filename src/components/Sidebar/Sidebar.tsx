import * as React from "react";

import "./styles/Sidebar.style.css";

interface Props {
  isVisible: boolean;
}

const divStyle = (props: Props): React.CSSProperties => ({
  width: props.isVisible ? "23rem" : "0rem",
});

export const Sidebar: React.StatelessComponent<Props> = (props) => (
  <div id="mySidenav" className="sidenav" style={divStyle(props)}>
    {props.children}
  </div>
);