import React from "react";
import { MemberEntity } from "./Model/member";
import { getMembersCollection } from "./api/memberAPI";

const useMemberCollection = () => {
    const [memberCollection, setMemberCollection] = React.useState<
        MemberEntity[]
    >([]);

    const loadMemberCollection = async () => {
        const memberCollection = await getMembersCollection();
        setMemberCollection(memberCollection);
    };

    return { memberCollection, loadMemberCollection };
};

export const TAMemberTable = () => {
    const { memberCollection, loadMemberCollection } = useMemberCollection();

    React.useEffect(() => {
        loadMemberCollection();
    });

    return (
        <>
            <table>
                <thead>
                    <tr>
                        <th>Avatar</th>
                        <th>Id</th>
                        <th>Name</th>
                    </tr>
                </thead>
                <tbody>
                    {memberCollection.map((member) => (
                        <MemberRow key={member.id} member={member} />
                    ))}
                </tbody>
            </table>
        </>
    );
};

const MemberRow = ({ member }: { member: MemberEntity }) => (
    <tr>
        <td>
            <img src={member.avatar_url} alt="" style={{ maxWidth: "10rem" }} />
        </td>
        <td>
            <span>{member.id}</span>
        </td>
        <td>
            <span>{member.login}</span>
        </td>
    </tr>
);