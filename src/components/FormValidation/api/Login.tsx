import { LoginEntity } from "../Model/Login";

export const isValidLogin = (loginInfo: LoginEntity): Promise<boolean> =>
    new Promise((resolve) => {
        setTimeout(() => {
            // mock call
            resolve(loginInfo.login === "admin" && loginInfo.password === "123");
        }, 500);
    });