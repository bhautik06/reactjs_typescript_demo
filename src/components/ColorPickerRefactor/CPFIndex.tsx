import * as React from "react";
import { CPFHello } from "./CPFHello";
import { CPFNameEdit } from "./CPFNameEdit";
import { CPFBrowser } from "./CPFBrowser";
import { CPF } from "./CPF";
import { Color } from "./Model/color";

export const CPFIndex = () => {
  const [name, setName] = React.useState("defaultUserName");
  const [editingName, setEditingName] = React.useState("defaultUserName");
  const [color, setColor] = React.useState<Color>({
    red: 20,
    green: 40,
    blue: 180
  });

  const loadUsername = () => {
    setTimeout(() => {
      setName("name from async call");
      setEditingName("name from async call");
    }, 500);
  };

  React.useEffect(() => {
    loadUsername();
  }, []);

  const setUsernameState = () => {
    setName(editingName);
  };

  return (
    <>
      <CPFBrowser color={color} />
      <CPF color={color} onColorUpdated={setColor}/>
      <CPFHello userName={name} />
      <CPFNameEdit
        initialUserName={name}
        editingName={editingName}
        onNameUpdated={setUsernameState}
        onEditingNameUpdated={setEditingName}
        disabled={editingName === "" || editingName === name}
      />
    </>
  );
};