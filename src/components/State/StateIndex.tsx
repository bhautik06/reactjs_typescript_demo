  
import React from "react";
import StateHello from "./StateHello";
import StateNameEdit from "./StateNameEdit";

const StateIndex = () => {
  const [name, setName] = React.useState("initialName");

  const setUsernameState = (event: React.ChangeEvent<HTMLInputElement>) => {
    setName(event.target.value);
  };

  return (
    <>
      <StateHello userName={name} />
      <StateNameEdit userName={name} onChange={setUsernameState} />
    </>
  );
};

export default StateIndex;