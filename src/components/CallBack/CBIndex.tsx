
import React from "react";
import CBHello from "./CBHello";
import CBNameEdit from "./CBNameEdit";

const CBIndex = () => {
  const [name, setName] = React.useState("defaultUserName");

  const setUsernameState = (newName: string) => {
    setName(newName);
  };

  return (
    <>
      <CBHello userName={name} />
      <CBNameEdit initialUserName={name} onNameUpdated={setUsernameState} />
    </>
  );
};

export default CBIndex;